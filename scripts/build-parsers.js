var mod = require('./index')

mod.versions.reduce(function (chain, version, ndx) {
  if (!ndx) {
    // chain = chain.then(() => mod.clean())
  }
  chain = chain.then(() => {
    return Promise.resolve()
    // .then(() => mod.fetch(version))
    // .then(() => mod.build(version))
    .then(() => mod.webpack(version))
    .then(() => mod.test(version))
    .then(() => mod.publish(version))
  })
  return chain
}, Promise.resolve())
