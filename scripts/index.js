var request = require('request')
var path = require('path')
var fs = require('fs-extra')
var gunzip = require('gunzip-maybe')
var tar = require('tar-fs')
var debug = require('debug')('parse-yarn-lock')
var webpack = require('webpack')
var pify = require('pify')
var execa = require('execa')
require('perish')

var PARSE_YARN_LOCK_ROOT = path.join(__dirname, '..')
var FORCE_FETCH = !!process.env.FORCE_FETCH
var YARN_SRC_DIST = path.join(PARSE_YARN_LOCK_ROOT, 'yarn-source')
var PARSERS_DIST = path.join(PARSE_YARN_LOCK_ROOT, 'parsers')
var PARSE_YARN_LOCK_LOCKFILE_PATH = path.join(PARSE_YARN_LOCK_ROOT, 'yarn.lock')
var PARSE_YARN_LOCK_PACKAGE_JSON_PATH = path.join(PARSE_YARN_LOCK_ROOT, 'package.json')
var PARSE_YARN_LOCK_INDEX = path.join(PARSE_YARN_LOCK_ROOT, 'index.js')

var VERSIONS = [
  'v0.23.0',
  'v0.24.6',
  'v0.25.4',
  'v0.26.1',
  'v0.27.5',
  'v0.28.4',
  // 'v1.0.0'
]
module.exports = {
  build: function (version) {
    var cwd = this.versionToPath(version)
    debug('installing ' + version + ' dependencies')
    var yarn = execa.shell('yarn install', { cwd: cwd })
    yarn.stdout.pipe(process.stdout)
    return yarn.then(function () {
      debug('building yarn ' + version)
      var yarnBuild = execa.shell('yarn run build', { cwd: cwd })
      yarnBuild.stdout.pipe(process.stdout)
      return yarnBuild
    })
  },
  clean: function (version) {
    debug('cleaning...')
    if (process.env.SKIP_CLEAN) {
      debug('...skipped')
      return Promise.resolve()
    }
    return Promise.all([fs.remove(PARSERS_DIST), fs.remove(YARN_SRC_DIST) ])
  },
  createYarnSourceDir: function () {
    return fs.mkdirp(YARN_SRC_DIST)
  },
  download (version) {
    var resolve
    var reject
    var deferred = new Promise (function (rs, rj) { resolve = rs; reject = rj }) // anti-pattern. sue me.
    var url = this.getDownloadUrl(version)
    debug('downloading version ' + version + ' from ' + url)
    var httpStream = request(url)
    .on('response', function(res) {
      if (!res.statusCode.toString().match(/20/)) return reject(new Error('bad download url: ' + res.body))
      res.pipe(gunzip()).pipe(tar.extract(YARN_SRC_DIST))
      .on('error', err => { return reject(err) })
      .on('finish', function () {
        debug('finished downloading: ' + version)
        return resolve()
      })
    })
    .on('error', reject)
    return deferred
  },
  getBuildTargetFile: function (version) {
    return path.join(PARSERS_DIST, version + '.js')
  },
  getDownloadUrl: function (version) {
    // https://github.com/yarnpkg/yarn/archive/v0.28.4.tar.gz
    return 'http://github.com/yarnpkg/yarn/archive/' + version + '.tar.gz'
  },
  publish: function (version) {
    debug('publishing ' + version)
    return fs.copy(this.getBuildTargetFile(version), PARSE_YARN_LOCK_INDEX)
    .then(() => fs.readFile(PARSE_YARN_LOCK_PACKAGE_JSON_PATH))
    .then(raw => {
      return fs.writeFile(PARSE_YARN_LOCK_PACKAGE_JSON_PATH + '.bak', raw)
      .then(() => raw)
    })
    .then(raw => JSON.parse(raw))
    .then(pkg => Object.assign(pkg, {
      version: version.replace('v', ''),
      main: 'index.json',
      files: [
        'index.js'
      ]
    }))
    .then(pkg => fs.writeFile(PARSE_YARN_LOCK_PACKAGE_JSON_PATH, JSON.stringify(pkg, null, 2)))
    .then(() => {
      return execa.shell('npm publish', { cwd: PARSE_YARN_LOCK_ROOT })
    })
    .then(() => fs.move(PARSE_YARN_LOCK_PACKAGE_JSON_PATH + '.bak', PARSE_YARN_LOCK_PACKAGE_JSON_PATH, { overwrite: true }))
    .then(() => debug('published ' + version))
  },
  versionToPath: function (version) {
    var versionDistPath = path.join(YARN_SRC_DIST, 'yarn-' + version.replace('v', ''))
    debug('version dist path: ' + versionDistPath)
    return versionDistPath
  },
  fetch: function (version) {
    return this.createYarnSourceDir()
    .then(function () {
      return fs.lstat(this.versionToPath(version))
      .then(function () { return true })
      .catch(function () { return false })
    }.bind(this))
    .then(function (isTarbalPresent) {
      if (isTarbalPresent === true && !FORCE_FETCH) {
        debug('skipping download of ' + version)
        return
      }
      var file = this.versionToPath(version)
      return fs.remove(file)
      .then(function () { return this.download(version) }.bind(this))
    }.bind(this))
  },
  test: function (version) {
    var mod = require(this.getBuildTargetFile(version))
    if (!mod || !mod.Parser || !mod.default) throw new Error('Parser not detected')
    var raw = fs.readFileSync(PARSE_YARN_LOCK_LOCKFILE_PATH).toString()
    var parsed = mod.default(raw)
    if (parsed) parsed = parsed.object || parsed
    if (!Object.keys(parsed).some(pkgName => pkgName.match(/perish/))) {
      throw new Error('parser build failed. unable to detect packages')
    }
  },
  webpack: function (version) {
    var root = this.versionToPath(version)
    var entry = path.join(root, './lib/lockfile/parse.js')
    return fs.lstat(entry)
    .then(function (stat) {
      debug('webpacking: ' + entry)
      var config = {
        context: root,
        entry: entry,
        target: 'node',
        output: {
          libraryTarget: 'commonjs',
          path: path.dirname(this.getBuildTargetFile(version)),
          filename: version + '.js'
        },
        module: {
          rules: [
            {
              test: [/\.js$/],
              exclude: [/node_modules/],
              loader: 'babel-loader',
              options: { presets: ['es2015'] }
            }
          ]
        }
      }
      return pify(webpack)(config)
    }.bind(this))
  },
  versions: VERSIONS
}
