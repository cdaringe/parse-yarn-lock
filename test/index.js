'use strict'

var fs = require('fs-extra')
var path = require('path')
var wf = require('run-waterfall')
var ava = require('ava').default
var versions = require('../scripts/index').versions
var parser = require(path.resolve(__dirname, '..', 'parsers', versions[versions.length - 1])).default

var LOCKFILE = path.resolve(__dirname, '..', 'yarn.lock')

ava('parsing', async function (t) {
  var buf = await fs.readFile(LOCKFILE)
  var parsed = parser(buf.toString())
  if (parsed) parsed = parsed.object || parsed
  t.truthy(parsed, 'parses string into something')
  t.truthy(typeof parsed === 'object', 'returns object')
  var keys = Object.keys(parsed)
  var hasDep = keys.some(pkgName => pkgName.match(/perish/))
  t.truthy(hasDep, 'has perish as dep')
})
