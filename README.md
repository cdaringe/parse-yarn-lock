# parse-yarn-lock

parse a `yarn.lock` file, using the the parser for a specific version of `yarn`.

[![build status](https://gitlab.com/cdaringe/parse-yarn-lock/badges/master/build.svg)](https://gitlab.com/cdaringe/parse-yarn-lock/commits/master) ![](https://img.shields.io/badge/standardjs-%E2%9C%93-brightgreen.svg) [![Build status](https://ci.appveyor.com/api/projects/status/k9wn1xsfe4202oo1?svg=true)](https://ci.appveyor.com/project/cdaringe/parse-yarn-lock)

## install

`yarn add parse-yarn-lock@0.28.4`, or `parse-yarn-lock@$VERSION`.  the following versions are currently supported:

```js
[
  'v0.23.0',
  'v0.24.6',
  'v0.25.4',
  'v0.26.1',
  'v0.27.5',
  'v0.28.4'
]
```

until further notice, please only install explicit versions.

## usage

```js
var parse = require('parse-yarn-lock').default
var lockfile = fs.readFileSync('yarn.lock').toString()
var parsed = parse(lockfile)
console.log(parsed) // parsed.object in yarn v0.28+
/*
{
  "balanced-match@^0.4.1": {
    "version": "0.4.2",
    "resolved": "https://registry.yarnpkg.com/balanced-match/-/balanced-match-0.4.2.tgz#cb3f3e3c732dc0f01ee70b403f302e61d7709838"
  },
  "brace-expansion@^1.0.0": {
    "version": "1.1.6",
    "resolved": "https://registry.yarnpkg.com/brace-expansion/-/brace-expansion-1.1.6.tgz#7197d7eaa9b87e648390ea61fc66c84427420df9",
    "dependencies": ...
  ...
}
*/
```
